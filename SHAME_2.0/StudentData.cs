﻿using System;

namespace StudentDatabase
{
    class Student
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Faculty { get; set; }
        public string Speciality { get; set; }
        public string Course { get; set; }
        public string Group { get; set; }

        public Student() { }
        public Student(string surname, string name, string patronymic, string faculty, string speciality, string course, string group)
        {
            Surname = surname;
            Name = name;
            Patronymic = patronymic;
            Faculty = faculty;
            Speciality = speciality;
            Course = course;
            Group = group;
        }

        public override string ToString() =>
            $"ФИО: {Surname} {Name} {Patronymic}\nФакультет: {Faculty}\nСпециальность: {Speciality}\nКурс: {Course}\nГруппа: {Group}";
    }
}