﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace StudentDatabase
{
    internal class Program
    {
        private static int stateMenu;
        private static List<Student> students = new List<Student>();
        private const string AutosaveFileName = "autosave.json";

        private static void Main(string[] args)
        {
            bool exitRequested = false;

            do
            {
                try
                {
                    Menu();

                    if (File.Exists(AutosaveFileName))
                    {
                        Load(AutosaveFileName);
                    }

                    while (stateMenu != 0)
                    {
                        switch (stateMenu)
                        {
                            case 1:
                                Console.Clear();
                                Console.WriteLine("1. Добавить ");
                                Console.WriteLine("2. Открыть файл");
                                int action = GetValidatedUserInputAsInteger("Ваш выбор: ", 1, 2);
                                Console.Clear();

                                if (action == 1)
                                {
                                    AddData();
                                    Save(AutosaveFileName);
                                }
                                else if (action == 2)
                                {
                                    DataReading();
                                    Save(AutosaveFileName);
                                }
                                else
                                {
                                    throw new Exception("Пункт меню выбран неверно, повторите попытку");
                                }

                                ClearConsoleAndReadLine();
                                break;

                            case 2:
                                Console.Clear();
                                if (students.Count > 0)
                                {
                                    Print();
                                }
                                else
                                {
                                    throw new Exception("Данные пусты!");
                                }

                                ClearConsoleAndReadLine();
                                break;

                            case 3:
                                Console.Clear();
                                if (students.Count > 0)
                                {
                                    Console.WriteLine("Сохранить файл");
                                    SavingData();
                                }
                                else
                                {
                                    throw new Exception("Данные пусты!");
                                }

                                ClearConsoleAndReadLine();
                                break;

                            case 4:
                                Console.Clear();
                                if (students.Count > 0)
                                {
                                    DeleteData();
                                    Save(AutosaveFileName);
                                }
                                else
                                {
                                    throw new Exception("Данные пусты!");
                                }

                                ClearConsoleAndReadLine();
                                break;

                            case 5:
                                Console.Clear();
                                if (students.Count > 0)
                                {
                                    DataChange();
                                    Save(AutosaveFileName);
                                }
                                else
                                {
                                    throw new Exception("Данные пусты!");
                                }

                                ClearConsoleAndReadLine();
                                break;

                            default:
                                throw new Exception("Пункт меню выбран неверно, повторите попытку");
                        }

                        Menu();
                    }

                    exitRequested = true;
                }
                catch (Exception ex)
                {
                    HandleError(ex.Message);
                }
            } while (!exitRequested);
        }

        private static void HandleError(string errorMessage)
        {
            Console.WriteLine(errorMessage);
            ClearConsoleAndReadLine();
        }

        private static void Menu()
        {
            Console.WriteLine("Выберите действие:");
            Console.WriteLine("======================================");
            Console.WriteLine("1. Добавить студента");
            Console.WriteLine("2. Посмотреть информацию");
            Console.WriteLine("======================================");
            Console.WriteLine("3. Сохранить файл");
            Console.WriteLine("4. Удалить студента");
            Console.WriteLine("5. Изменить");
            Console.WriteLine("======================================");
            Console.Write("Ваш выбор: ");
            stateMenu = GetValidatedUserInputAsInteger();
        }

        private static void AddData()
        {
            students.Add(new Student
            {
                Surname = InputNoSpaces("Фамилия: "),
                Name = InputNoSpaces("Имя: "),
                Patronymic = InputNoSpaces("Отчество: "),
                Faculty = InputNoSpaces("Название факультета: "),
                Speciality = InputNoSpaces("Название специальности: "),
                Course = InputNoSpaces("Номер курса: "),
                Group = InputNoSpaces("Номер группы: ")
            });

            Console.Clear();
            Console.WriteLine("Данные добавлены!");
        }

        private static void Load(string fileName)
        {
            string json = File.ReadAllText(fileName, Encoding.UTF8);
            List<Student> loadedStudents = JsonSerializer.Deserialize<List<Student>>(json);

            students.Clear();
            students.AddRange(loadedStudents);
        }

        private static void DataReading()
        {
            string fileName = GetUserInput("Введите название файла: ");
            if (File.Exists(fileName))
            {
                Load(fileName);
                Console.Clear();
                Console.WriteLine($"Файл {fileName} успешно открыт.");
            }
            else
            {
                throw new Exception($"Файл {fileName} не существует.");
            }
        }

        private static void Print()
        {
            int i = 1;
            foreach (Student student in students)
            {
                Console.WriteLine($"Данные №{i++}");
                Console.WriteLine(student);
                Console.WriteLine("====================================");
            }
        }

        private static void SavingData()
        {
            string fileName = GetUserInput("Введите название файла: ");
            SaveToFile(fileName);
        }

        private static void SaveToFile(string fileName)
        {
            Console.Clear();
            if (Save(fileName))
            {
                Console.WriteLine($"Данных: {students.Count} записано в файл: {fileName}");
            }
        }

        private static void DataChange()
        {
            int n = GetValidatedUserInputAsInteger($"Введите необходимый элемент (от 1 до {students.Count}): ", 1, students.Count) - 1;
            Console.Clear();

            students[n] = new Student(
                InputNoSpaces("Фамилия: "),
                InputNoSpaces("Имя: "),
                InputNoSpaces("Отчество: "),
                InputNoSpaces("Название факультета: "),
                InputNoSpaces("Название специальности: "),
                InputNoSpaces("Номер курса: "),
                InputNoSpaces("Номер группы: ")
            );

            Console.Clear();
            Console.WriteLine($"Данные элемента: {n + 1} изменены!");
        }

        private static void DeleteData()
        {
            int n = GetValidatedUserInputAsInteger($"Введите номер элемента для удаления (от 1 до {students.Count}): ", 1, students.Count) - 1;
            Console.Clear();

            students.RemoveAt(n);

            Console.Clear();
            Console.WriteLine($"Данные №{n + 1} удалены!");
        }

        private static bool Save(string fileName)
        {
            var options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic),
                WriteIndented = true
            };

            string jsonString = JsonSerializer.Serialize(students, options);
            File.WriteAllText(fileName, jsonString, Encoding.UTF8);
            return File.Exists(fileName);
        }

        private static string InputNoSpaces(string message)
        {
            string res;
            do
            {
                Console.Write(message);
                res = Console.ReadLine();
                res = res.Trim();
                Console.Clear();
            } while (res == "");

            return res;
        }

        private static string GetUserInput(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        private static int GetValidatedUserInputAsInteger(string message = "", int min = 1, int max = int.MaxValue)
        {
            int userInput;
            bool isValidInput = false;
            int originalLeft = Console.CursorLeft;
            int originalTop = Console.CursorTop;

            do
            {
                Console.SetCursorPosition(originalLeft, originalTop);
                Console.Write(new string(' ', Console.WindowWidth - originalLeft));
                Console.SetCursorPosition(originalLeft, originalTop);
                Console.Write(message);
                string input = Console.ReadLine();
                if (int.TryParse(input, out userInput))
                {
                    if (userInput >= min && userInput <= max)
                    {
                        isValidInput = true;
                    }
                    else
                    {
                        throw new Exception(string.Format("Некорректный ввод, введите целое число от {0} до {1}", min, max));
                    }
                }
                else
                {
                    throw new Exception("Некорректный ввод, введите целое число");
                }
            } while (!isValidInput);

            return userInput;
        }

        private static void ClearConsoleAndReadLine()
        {
            Console.ReadLine();
            Console.Clear();
        }
    }
}